'''
Unittest the function pool module
'''
import unittest
import hfcl.core.function_pool as fp
from operator import add, sub


class FunctionPoolTester(unittest.TestCase):
    def test_add_infix_operation(self):
        fp.add_infix_operation('plus', float, float, add, 2)
        self.assertEqual(fp.INFIX_OPERATIONS['plus'][(float, float)], (add, 2))
        self.assertRaises(fp.FunctionAlreadyExistsException,
                          fp.add_infix_operation, 'plus', float, float, add, 2)
        # Clear all function to not interfere with the other tests
        fp.INFIX_OPERATIONS = {}

    def test_add_function(self):
        def add_three(x, y, z):
            return x + y + z
        fp.add_function('plus', (float, float), add)
        self.assertEqual(fp.FUNCTIONS['plus'][(float, float)], add)
        self.assertRaises(fp.FunctionAlreadyExistsException,
                          fp.add_function, 'plus', (float, float), add)
        fp.add_function('plus', (float, float, float), add_three)
        self.assertEqual(fp.FUNCTIONS['plus'][(float, float, float)],
                         add_three)
        # Clear all function to not interfere with other tests
        fp.FUNCTIONS = {}

    def test_call_infix_operation(self):
        # Clear all operations and add one manually to be independend
        # from the other testcases
        fp.INFIX_OPERATIONS = {}
        fp.INFIX_OPERATIONS['test'] = {}
        fp.INFIX_OPERATIONS['test'][(float, float)] = (add, 2)
        fp.INFIX_OPERATIONS['test'][(int, int)] = (sub, 2)
        self.assertEqual(fp.call_infix_operation('test', 1.0, 1.0), 2.0)
        self.assertEqual(fp.call_infix_operation('test', 1, 1), 0)
        # Clear all function to not interfere with the other tests
        fp.INFIX_OPERATIONS = {}

    def test_call_function(self):
        # Clear all operations and add one manually to be independend
        # from the other testcases
        fp.FUNCTIONS = {}
        fp.FUNCTIONS['test'] = {}
        fp.FUNCTIONS['test'][(float, float)] = add
        fp.FUNCTIONS['test'][(int, int)] = sub
        self.assertEqual(fp.call_function('test', (1.0, 1.0)), 2.0)
        self.assertEqual(fp.call_function('test', (1, 1)), 0)
        # Clear all function to not interfere with the other tests
        fp.FUNCTIONS = {}

    def test_infix_dekorator(self):
        fp.INFIX_OPERATIONS = {}

        @fp.publish_infix(float, float, 2)
        def plus(lvalue, rvalue):
            return lvalue + rvalue

        @fp.publish_infix(float, float, 2, '<>')
        def plus2(lvalue, rvalue):
            return lvalue + rvalue

        self.assertEqual(fp.INFIX_OPERATIONS['plus'][(float, float)], (plus, 2))
        self.assertEqual(fp.INFIX_OPERATIONS['<>'][(float, float)], (plus2, 2))
        self.assertEqual(fp.call_infix_operation('<>', 1.0, 2.0), 3)
        # Clear all function to not interfere with the other tests
        fp.FUNCTIONS = {}

    def test_function_dekorator(self):
        from math import e
        fp.FUNCTIONS = {}

        @fp.publish_function(float)
        def exp(value):
            return e**value

        self.assertEqual(fp.FUNCTIONS['exp'][(float, )], exp)

        fp.FUNCTIONS = {}


if __name__ == '__main__':
    unittest.main()
