'''
Unittest the unit.py module
'''
import unittest
import hfcl.core.unit as unit


class UnitTester(unittest.TestCase):
    def test_addition(self):
        second = unit.Unit(0, 1, 0, 0, 0, 0, 0, 0)
        meter = unit.Unit(0, 0, 1, 0, 0, 0, 0, 0)
        self.assertEqual(unit.add(second, second), second,
                         'Should be seconds')
        self.assertRaises(unit.MissmatchingUnitsException,
                          unit.add,
                          second,
                          meter)
        unit.set_default_extended_base_units([])

    def test_subtraction(self):
        second = unit.Unit(0, 1, 0, 0, 0, 0, 0, 0)
        meter = unit.Unit(0, 0, 1, 0, 0, 0, 0, 0)
        self.assertEqual(unit.sub(second, second), second,
                         'Should be seconds')
        self.assertRaises(unit.MissmatchingUnitsException,
                          unit.sub,
                          second,
                          meter)
        unit.set_default_extended_base_units([])

    def test_multiplikatio(self):
        second = unit.Unit(0, 1, 0, 0, 0, 0, 0, 0)
        meter = unit.Unit(0, 0, 1, 0, 0, 0, 0, 0)
        second_meter = unit.Unit(0, 1, 1, 0, 0, 0, 0, 0)
        self.assertEqual(unit.mul(second, meter), second_meter,
                         'Should be second*meter')
        self.assertEqual(unit.mul(meter, second), second_meter,
                         'Should be second*meter')
        unit.set_default_extended_base_units([])

    def test_division(self):
        second = unit.Unit(0, 1, 0, 0, 0, 0, 0, 0)
        per_meter = unit.Unit(0, 0, -1, 0, 0, 0, 0, 0)
        second_per_meter = unit.Unit(0, 1, -1, 0, 0, 0, 0, 0)
        self.assertEqual(unit.mul(second, per_meter), second_per_meter,
                         'Should be second/meter')
        unit.set_default_extended_base_units([])

    def test_exponentiation(self):
        unit_e1 = unit.Unit(0, 1, 0, 1, 0, 1, 0, 1)
        unit_e4 = unit.Unit(0, 4, 0, 4, 0, 4, 0, 4)
        self.assertEqual(unit.exp(unit_e1, 4), unit_e4,
                         f'Should be {unit_e4}')
        unit.set_default_extended_base_units([])

    def simplify_and_expanse(self):
        meter = unit.Unit(0, 0, 1, 0, 0, 0, 0, 0)
        newton = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        work = unit.Unit(1, -2, 2, 1, 0, 0, 0, 0, [newton])
        work.simplify_inplace()
        self.assertEqual(work, meter, 'Work should be reduced to meter'
                                      'in si representation')
        work.expand_inplace()
        self.assertEqual(work, unit.mul(newton, meter))
        unit.set_default_extended_base_units([])

    def test_set_default_extended_base_units(self):
        newton = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        unit.set_default_extended_base_units([newton])
        work = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        self.assertEqual(work.extended_base_units, [newton],
                         'Extended base units should be [newton]')
        unit.set_default_extended_base_units([])

    def test_unit_from_unit(self):
        newton = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        unit.set_default_extended_base_units([newton])
        meter = unit.Unit(0, 0, 1, 0, 0, 0, 0, 0)
        meter2 = unit.unit_from_unit(meter)
        self.assertEqual(meter, meter2, 'meter should be equal to meter2')
        self.assertEqual(meter.extended_base_units, [newton],
                         'Extended base units should be [newton]')
        unit.set_default_extended_base_units([])

    def test_unit_from_repr(self):
        newton = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        unit.set_default_extended_base_units([newton])
        meter = unit.Unit(0, 0, 1, 0, 0, 0, 0, 0)
        meter2 = unit.unit_from_repr(meter.repr)
        self.assertEqual(meter, meter2, 'meter should be equal to meter2')
        self.assertEqual(meter.extended_base_units, [newton],
                         'Extended base units should be [newton]')
        unit.set_default_extended_base_units([])

    def test_set_extended_base_units(self):
        newton = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        kilo_gramm_meter = unit.Unit(1, 0, 1, 1, 0, 0, 0, 0)
        unit.set_default_extended_base_units([newton])
        newton2 = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        newton2.set_extended_base_units([kilo_gramm_meter])
        self.assertEqual(newton2.extended_base_units, [kilo_gramm_meter])
        self.assertEqual(newton2, unit.Unit(0, -2, 0, 0, 0, 0, 0, 0))
        self.assertEqual(newton2.extended_repr, [1])
        unit.set_default_extended_base_units([])

    def test_simplify_priority(self):
        newton = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        kilo_gramm_meter = unit.Unit(1, 0, 1, 1, 0, 0, 0, 0)
        unit.set_default_extended_base_units([newton, kilo_gramm_meter])
        newton2 = unit.Unit(1, -2, 1, 1, 0, 0, 0, 0)
        newton2.simplify_inplace()
        self.assertEqual(newton2.repr, (0, 0, 0, 0, 0, 0, 0, 0))
        self.assertEqual(newton2.extended_repr, [1, 0])
        newton2.set_extended_base_units([kilo_gramm_meter, newton])
        self.assertEqual(newton2.repr, (0, -2, 0, 0, 0, 0, 0, 0))
        self.assertEqual(newton2.extended_repr, [1, 0])
        unit.set_default_extended_base_units([])


if __name__ == '__main__':
    unittest.main()
