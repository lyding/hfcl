'''
Unittest the lang.py module
(Requieres the function pool to be tested)
'''
import unittest
import hfcl.core.function_pool as fp
from hfcl.core.lang import EVALUATING_PARSER
from hfcl.core.unit import Unit
from hfcl.basic.unit_scalar import UnitScalar


class UnitTester(unittest.TestCase):
    def test_function_call(self):
        @fp.publish_function(UnitScalar)
        def square(value):
            return UnitScalar(value.value**2, value.unit)

        parsed = EVALUATING_PARSER.parse('square(4)')
        print(parsed)
        self.assertEqual(parsed.value, 16, 'Value should be 16')
        self.assertEqual(parsed.unit, Unit(),
                         'Unit should be zeroes')


if __name__ == '__main__':
    unittest.main()
