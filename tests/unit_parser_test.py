'''
Unittest the unit_parser.py module
'''
import unittest
import hfcl.core.unit as unit
from hfcl.core.unit_parser import UNIT_PARSER
from lark import Lark


class UnitParserTester(unittest.TestCase):
    def test_parsing(self):
        UNIT_PARSER
        cmd = 'kgms^-2'
        parsed_unit = UNIT_PARSER.parse(cmd)
        newton = unit.Unit(1, -2, 1, 1)
        self.assertEqual(parsed_unit, newton)


if __name__ == '__main__':
    unittest.main()
