'''
This little script explores the possibility to parenthese
an equation correctly according to the operator precedences
'''
from lark import Lark, Transformer


# A grammar which parses from right to left without
# operator precedence, this is the testcase for
# the parenthesis algorithm
# After setting the parenthesis the parser should evaluate
# the string to the expected result, aka the result
# with operator precedence
grammar = '''
    ?start: expr
    expr: [expr OPERATOR] (value | "(" expr ")") -> evaluate_expr
    OPERATOR: "+"
        | "-"
        | "*"
        | "/"
    value: SIGNED_NUMBER -> evaluate_value
    %import common.SIGNED_NUMBER
    %import common.WS
    %ignore WS
    '''


class TreeTransformer(Transformer):
    from operator import add, sub, mul, truediv as div

    def __init__(self):
        self.function_table = {
            '+': self.add,
            '-': self.sub,
            '*': self.mul,
            '/': self.div}

    def evaluate_value(self, args):
        return float(args[0])

    def evaluate_expr(self, args):
        if len(args) == 1:
            return args[0]
        return self.function_table[args[1]](args[0], args[2])


def set_parentheses(input_string):
    result = '(((('
    for char in input_string:
        result += '((((' if char == '(' else ''
        result += '))))' if char == ')' else ''
        result += ')^(' if char == '^' else ''
        result += '))' + char + '((' if char in '*/' else ''
        result += ')))' + char + '(((' if char in '+-' else ''
        result += char if char not in '()*/+-' else ''
    result += '))))'
    return result


if __name__ == '__main__':
    # Here the string has to be reverted
    # as the parser is parsing from right to left...
    cmd = '1 + 2 * 3'
    parser = Lark(grammar, parser='lalr', transformer=TreeTransformer())
    transformed_cmd = set_parentheses(cmd)
    print(cmd)
    print(transformed_cmd)
    print('Parsed untransformed:', parser.parse(cmd))
    print('Parsed transformed:', parser.parse(transformed_cmd))
