The situation
--------------
Languages with custom infox operations without
naming convention for different precedence levels
are a little bit harder to parse as the operator
precedence cant be descirbed in the languages grammar.
Therefor I had two different ideas:

Extend the grammar dynamically
---------------------------------
My first idea was to hardcode X level of precedences
in the languages grammar and then add the defined operations
according to theire precedence in the gramar of the language.
This should work but seemed to be ugly, also operators are
allowed to be overloaded for different types and this
approach would not be flexible enough for different precedences
for overloaded definitions of an operator.
(Even if I am quite sure that this is a bad idea to do)

Parse the expression according to the operators precedence
-------------------------------------------------------------
I read something about adding the right parenthesis for operators,
like it was used in the early fortran compilers.
This way a preprocessor step could be introduced in which the
parenthesis are set accordingly to the precedence of the operators.
The underlying grammar of the language than would not need handle
operator precedence in any way but parse all operators from left
to right. This can be easily implemented using the algorithm
as it is described here: https://en.wikipedia.org/wiki/Operator-precedence_parser
but would not allow overloaded operators with different precedences as well,
except the parsing end transformation process are realy intertwined into
a parsing process aware of the type of the expressions.

Cause overloaded infix operations with different precedences seems to be
quite hard to implement and are of questionable use, I came to the
conclusion to use the second methode without overloaded operators
of different precedences.

Conclusion:
-----------
As the small script 'parenthesis.py' demonstrates is it possible to
create a small grammar which does not have any kind of operator precedence
but evaluates all operators from left to right, but the second methode
is suitable enough to overcome this by adding the correct parenthesis.
This will be the methode I am going to use in what is still called ppe
(standing for python physics environment) even if its not a python
environment anymore.

Misc:
-----
Reading documentary is crucial, it revealed:
	1. rules in lark are written in lower case, terminals in uppercase
	   this MAKES A DIFFERENCE in how the parse tree is generated

Problem:
--------
As operator names do not follow any naming conventions its not trivial to
apply the second method to this case. Therefor a new grammar is defined
which is only about parsing the operators which then are looked up
and extended with parenthesis. This is outdated...
While waiting for the train I had a new idea, Operators which consist only
of special characters are easy distinguishable and therefor do not
need a space between them and the operands.
All operators which do not consist only of special characters need
a whitespace between them and the operand, otherwise it would not
be distuingishable for the programmer as shown in the following
example:

	x**2 -> clear
	v1 cross v2 -> clear
	v1cross v2 -> unclear
	v1 crossv2 -> unclear
	v1crossv2 -> probably just a variable with the name v1crossv2 -> unclear
