'''
This module defines a little evaluator which evaluates
a parsed arithmetic expression from left to right regardless
of any precedence
'''
from lark import Lark, Transformer
from math import sin, cos
from operator import add, sub, mul, truediv as div


grammar = '''
    ?start: expr -> evaluate_start
        | CNAME "=" expr -> evaluate_assign
    ?expr: expr OPERATOR factor -> evaluate_infix_operation_call
        | factor
    ?factor: SIGNED_NUMBER [unit]-> evaluate_number
        | "(" expr ")" -> evaluate_parenthesed_expr
        | function
        | variable
    ?function: CNAME parameter_list -> evaluate_function
    ?variable: CNAME -> evaluate_variable
    ?unit: (base_unit)+ -> evaluate_unit
    ?base_unit: LETTER ["**" SIGNED_NUMBER] -> evaluate_base_unit
    parameter_list: "(" [expr ("," expr)*] ")" -> evaluate_parameter_list
    OPERATOR: (/[^a-zA-Z0-9()=\\s]/)+ | " " CNAME " "
    %import common.CNAME
    %import common.LETTER
    %import common.SIGNED_NUMBER
    %import common.WS_INLINE
    %ignore WS_INLINE
    '''


def exp(base, exp):
    return base**exp


def average(*args):
    result = 0
    for arg in args:
        result += arg
    result /= len(args)
    return result


OPERATOR_TABLE = {
    '+': (add, 2),
    '-': (sub, 2),
    '*': (mul, 1),
    '/': (div, 1),
    'exp': (exp, 0),
}

FUNCTION_TABLE = {
    'sin': sin,
    'cos': cos,
    'average': average
}


class EvaluatePrecedenceFree(Transformer):
    def __init__(self):
        self.variables = {}

    def evaluate_start(self, args):
        return args[0]

    def evaluate_assign(self, args):
        self.variables[str(args[0])] = args[1]
        return args[1]

    def evaluate_infix_operation_call(self, args):
        operation = OPERATOR_TABLE[args[1].strip()][0]
        return operation(args[0], args[2])

    def evaluate_parenthesed_expr(self, args):
        return args[0]

    def evaluate_number(self, args):
        return float(args[0])

    def evaluate_function(self, args):
        f = FUNCTION_TABLE[args[0]]
        params = args[1]
        return f(*params)

    def evaluate_parameter_list(self, args):
        return args

    def evaluate_variable(self, args):
        return self.variables[str(args[0])]


class ParenthesisTransformer(Transformer):
    def evaluate_start(self, args):
        return '((((' + args[0] + '))))'

    def evaluate_assign(self, args):
        return args[0] + '=((((' + args[1] + '))))'

    def evaluate_number(self, args):
        '''
        Just returns the representing string
        '''
        if len(args) > 1:
            return args[0] + args[1]
        return args[0]

    def evaluate_function(self, args):
        return args[0] + args[1]

    def evaluate_parameter_list(self, args):
        result = '('
        for arg in args[:-1]:
            result += arg + ', '
        result += args[-1] + ')'
        return result

    def evaluate_variable(self, args):
        return str(args[0])

    def evaluate_infix_operation_call(self, args):
        '''
        Return the operators name pre- and postpended with the
        right amount of parenthesis according to the operators
        precedence
        '''
        p = OPERATOR_TABLE[args[1].strip()][1]
        return args[0] + ')' * p + args[1] + '(' * p + args[2]

    def evaluate_parenthesed_expr(self, args):
        '''
        Return the operators name pre- and postpended with the
        right amount of parenthesis according to the operators
        precedence
        '''
        p = OPERATOR_TABLE[args[1].strip()][1]
        return '((((' + args[0] + ')' * p + args[1] + '(' * p\
               + args[2] + '))))'

    def evaluate_base_unit(self, args):
        return args[0]

    def evaluate_unit(self, args):
        result = ''
        for arg in args:
            result += arg
        return result


if __name__ == '__main__':
    # cmd = '1+2 exp 3'
    # cmd = '1 + sin(1) exp 2'
    cmd = 'x = 5 + 2 exp 2'
    cmd1 = 'x + 3'
    evaluater = EvaluatePrecedenceFree()
    evaluating_parser = Lark(grammar,
                             parser='lalr',
                             transformer=evaluater)
    transforming_parser = Lark(grammar,
                               parser='lalr',
                               transformer=ParenthesisTransformer())
    transformed_cmd = transforming_parser.parse(cmd)
    transformed_cmd1 = transforming_parser.parse(cmd1)
    print(transformed_cmd)
    print(transformed_cmd1)
    print(cmd, '->', evaluating_parser.parse(transformed_cmd))
    print(cmd1, '->', evaluating_parser.parse(transformed_cmd1))
