'''
This little script explores the possibility to parenthese
an equation correctly according to the operator precedences
'''
from lark import Lark, Transformer


# A grammar which parses from right to left without
# operator precedence, this is the testcase for
# the parenthesis algorithm
# After setting the parenthesis the parser should evaluate
# the string to the expected result, aka the result
# with operator precedence
grammar = '''
    ?start: expr
    ?expr: [expr OPERATOR] SIGNED_NUMBER  -> transform_expr
    OPERATOR: (/[^a-zA-Z0-9()=\\s]/)+ | " " CNAME " "
    %import common.SIGNED_NUMBER
    %import common.WS_INLINE
    %import common.CNAME
    %ignore WS_INLINE
    '''


class ParenthesisTransformer(Transformer):
    def __init__(self):
        self.operator_precedence_dict = {
            '+': 3,
            '<>': 1,
            'foo': 4}

    def transform_expr(self, args):
        result = ''
        for arg in args:
            try:
                if arg.type == 'OPERATOR':
                    operator_name = arg.strip()
                    p = self.operator_precedence_dict[operator_name]
                    result += ')' * p + operator_name + '(' * p
                    continue
            except AttributeError:
                pass
            result += str(arg)

        return result


def set_parentheses(input_string):
    result = '(((('
    for char in input_string:
        result += '((((' if char == '(' else ''
        result += '))))' if char == ')' else ''
        result += ')^(' if char == '^' else ''
        result += '))' + char + '((' if char in '*/' else ''
        result += ')))' + char + '(((' if char in '+-' else ''
        result += char if char not in '()*/+-' else ''
    result += '))))'
    return result


if __name__ == '__main__':
    # Here the string has to be reverted
    # as the parser is parsing from right to left...
    cmd = '1<>2 foo 4'
    parser = Lark(grammar, parser='lalr', transformer=ParenthesisTransformer())
    # parser = Lark(grammar, parser='lalr')
    transformed_cmd = set_parentheses(cmd)
    print(cmd)
    print('Parsed untransformed:', '(((((' + parser.parse(cmd) + ')))))')
