'''
This module contains trigonometric functions
'''

from hfcl.basic.unit_scalar import UnitScalar
from hfcl.core.unit import Unit, MissmatchingUnitsException
from mpmath import mp
import hfcl.core.function_pool as fp


@fp.publish_function(UnitScalar)
def sin(x):
    '''Calculates the sin in radians'''
    if x.unit.is_scalar():
        return UnitScalar(mp.sin(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())


@fp.publish_function(UnitScalar)
def cos(x):
    '''Calculates the cos in radians'''
    if x.unit.is_scalar():
        return UnitScalar(mp.cos(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())


@fp.publish_function(UnitScalar)
def tan(x):
    '''Calculates the tan in radians'''
    if x.unit.is_scalar():
        return UnitScalar(mp.tan(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())


@fp.publish_function(UnitScalar)
def asin(x):
    '''Calculates the asin in radians'''
    if x.unit.is_scalar():
        return UnitScalar(mp.asin(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())


@fp.publish_function(UnitScalar)
def acos(x):
    '''Calculates the acos in radians'''
    if x.unit.is_scalar():
        return UnitScalar(mp.acos(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())


@fp.publish_function(UnitScalar)
def atan(x):
    '''Calculates the atan in radians'''
    if x.unit.is_scalar():
        return UnitScalar(mp.atan(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())


@fp.publish_function(UnitScalar)
def degrees(x):
    '''Converts radians in degrees'''
    if x.unit.is_scalar():
        return UnitScalar(mp.degrees(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())


@fp.publish_function(UnitScalar)
def radians(x):
    '''Converts degrees in radians'''
    if x.unit.is_scalar():
        return UnitScalar(mp.radians(x.value))
    raise MissmatchingUnitsException(x.unit, Unit())
