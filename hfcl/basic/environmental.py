'''
This module contains functions which are usefull to interact with
the hfcl environment
'''
import hfcl.core.function_pool as fp
import hfcl.core.lang as lang
import hfcl.core.unit_parser as up
import hfcl.core.config as config
from hfcl.core.base_object import String
from hfcl.core.eco import execute_hfcl_module, import_hfcl_plugin


@fp.publish_function()
def show_variables():
    '''
    Print out a table of all defined variables
    '''
    print("{:<8} {:<30} {:<10}".format('Name', 'Value', 'Type'))
    for variable_entry in lang.PRECEDENCE_FREE_TRANSFORMER.variables.items():
        name, value = variable_entry
        print("{:<8} {:<30} {:<10}".format(
            str(name), str(value), str(type(value))))


@fp.publish_function()
def show_functions():
    '''
    Print out a table of all defined functions
    '''
    print("{:<8} {:<50} {}".format('Name', 'Signature', 'Docstring'))
    for name in fp.FUNCTIONS:
        signatures = list(fp.FUNCTIONS[name].keys())
        functions = list(fp.FUNCTIONS[name].values())
        for i in range(len(functions)):
            docstring = functions[i].__doc__
            if docstring:
                docstring = docstring.strip()
            else:
                docstring = ''
            print("{:<8} {:<50} {}".format(name,
                  str(signatures[i]), docstring))


@fp.publish_function()
def show_units():
    '''
    Print out a table of all defined units
    '''
    print("{:<8} {:<20}".format('Unit', 'Representation'))
    for item in up.UNIT_EVALUATOR.unit_map.items():
        print("{:<8} {:<20}".format(str(item[0]), str(item[1])))


@fp.publish_function(String, String)
def define_unit(unit_name, unit_repr):
    '''
    Define a unit with the already defined units
    '''
    print('Defining unit:', unit_repr)
    up.UNIT_EVALUATOR.unit_map[unit_name] = up.UNIT_PARSER.parse(unit_repr)


@fp.publish_function(String)
def load(module_name):
    '''
    Load a hfcl module from the subdirectory
    '''
    execute_hfcl_module(module_name, config.PLUGIN_FILE_SUBDIRECTORY)


@fp.publish_function_named('import', String)
def import_plugin(plugin_name):
    '''
    Import a python plugin for the hfcl language
    '''
    import_hfcl_plugin(plugin_name)
