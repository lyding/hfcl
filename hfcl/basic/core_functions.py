'''
This module contains core functions and operators
which have to be defined for hfcl to work properly
'''
from hfcl.core.base_object import BaseObject
from hfcl.core.function_pool import publish_infix


@publish_infix(BaseObject, BaseObject, 4, ',')
@publish_infix(list, BaseObject, 4, ',')
@publish_infix(BaseObject, list, 4, ',')
def list_operator(operand1, operand2):
    op1 = [operand1] if type(operand1) is not list else operand1
    op2 = [operand2] if type(operand2) is not list else operand2
    return op1 + op2


if __name__ == '__main__':
    print(list_operator(1, 2))
    print(list_operator([1], 2))
    print(list_operator(1, [2]))
    print(list_operator([1], [2]))
