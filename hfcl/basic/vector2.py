'''
This module contains a two dimensional vector of numericals
'''
from hfcl.core.base_object import BaseObject, Numerical
from hfcl.core.function_pool import (publish_infix,
                                     publish_function,
                                     publish_function_named)
from hfcl.core.exceptions import NotDefined
from hfcl.basic.unit_scalar import UnitScalar
from mpmath import mp


class Vector2(BaseObject):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vector2(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector2(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        '''
        Dot product
        '''
        return self.x * other.x + self.y * other.y

    def __str__(self):
        return f'({str(self.x)}, {str(self.y)})'

    def get_unit_tuple(self):
        return self.x.unit, self.y.unit


@publish_function_named('vec2', Numerical)
def vec2_constructor1(x):
    '''Constructs a two dimension vector, where x and y are equal'''
    return Vector2(x, x)


@publish_function_named('vec2', Numerical, Numerical)
def vec2_constructor2(x, y):
    '''Constructs a two dimensional vector'''
    return Vector2(x, y)


@publish_infix(Vector2, Vector2, 2, '+')
def add(v1, v2):
    return v1 + v2


@publish_infix(Vector2, Vector2, 2, '-')
def sub(v1, v2):
    return v1 - v2


@publish_infix(Vector2, Vector2, 1, '*')
def dot(v1, v2):
    return v1 * v2


@publish_infix(Vector2, UnitScalar, 1, '*', True)
def mul(v1, f):
    return Vector2(v1.x * f, v1.y * f)


@publish_infix(Vector2, UnitScalar, 1, '/', True)
def div(v1, f):
    return Vector2(v1.x / f, v1.y / f)


@publish_infix(Vector2, Vector2, 3, '||')
def parallel(v1, v2):
    return (v1 * v2).value == (abs(v1) * abs(v2)).value


@publish_infix(Vector2, Vector2, 3, 'T')
def perpendicular(v1, v2):
    return (v1 * v2).value == 0


@publish_infix(Vector2, Vector2, 3, 'V')
@publish_function_named('arg', Vector2, Vector2)
def angle(v1, v2):
    '''Calculates the angle between two vectors, equal to the V-operator'''
    if v1.get_unit_tuple() != v2.get_unit_tuple():
        raise NotDefined('The angle of two vectors with '
                         'differing units is not defined')
    angle = mp.acos(((v1 * v2) / (abs(v1) * abs(v2))).value)
    return UnitScalar(angle)


@publish_function(Vector2)
def abs(v1):
    '''Calculates the the length of a vector'''
    if v1.x.unit != v1.y.unit:
        raise NotDefined('abs not defined on vector with differing units')
    return UnitScalar(mp.sqrt(v1.x.value**2 + v1.y.value**2), v1.x.unit)


@publish_function(Vector2)
def normalise(v1):
    '''Normalize a vector'''
    length = abs(v1)
    return Vector2(v1.x / length, v1.y / length)
