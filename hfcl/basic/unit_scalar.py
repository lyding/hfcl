'''
Unit scalars are the basic representation of scalars in hfcl
Its a composition of a normal float and an unit instance.
'''
from hfcl.core.unit import (Unit,
                            sqrt as unit_sqrt,
                            exp as unit_exp)
from hfcl.core.function_pool import publish_function_named, publish_infix
from hfcl.core.base_object import Numerical, Comparable
from hfcl.core.exceptions import NotDefined
import mpmath

# Set the decimals precision to 200
# Isch will dir ma was sage, schen is des net!
mpmath.mp.dps = 200
mpmath.mp.pretty = True


class UnitScalar(Numerical, Comparable):
    def __init__(self, value=0, unit=Unit()):
        self.value = mpmath.mpc(value)
        self.unit = unit
        self.complex = self.value.imag != 0

    def __str__(self):
        return f'{self.value} {str(self.unit)}'

    def __add__(self, other):
        return UnitScalar(self.value + other.value,
                          self.unit + other.unit)

    def __sub__(self, other):
        return UnitScalar(self.value - other.value,
                          self.unit - other.unit)

    def __mul__(self, other):
        if not self.is_scalar() and other.complex or\
           not other.is_scalar() and self.complex:
            raise NotDefined('Multiplication for units and complex numbers '
                             'is not defined')
        return UnitScalar(self.value * other.value,
                          self.unit * other.unit)

    def __truediv__(self, other):
        if not self.is_scalar() and other.complex or\
           not other.is_scalar() and self.complex:
            raise NotDefined('Division for units and complex numbers '
                             'is not defined')
        return UnitScalar(self.value / other.value,
                          self.unit / other.unit)

    def __lt__(self, other):
        if self.complex or other.complex:
            raise NotDefined('Comparing complex values is not defined')
        if self.unit != other.unit:
            raise NotDefined('Comparing unit scalars with differing units'
                             'is not defined')
        return self.value < other.value

    def __le__(self, other):
        if self.complex or other.complex:
            raise NotDefined('Comparing complex values is not defined')
        if self.unit != other.unit:
            raise NotDefined('Comparing unit scalars with differing units'
                             'is not defined')
        return self.value <= other.value

    def __eq__(self, other):
        if self.unit != other.unit:
            raise NotDefined('Comparing unit scalars with differing units'
                             'is not defined')
        return self.value == other.value

    def __ge__(self, other):
        if self.complex or other.complex:
            raise NotDefined('Comparing complex values is not defined')
        if self.unit != other.unit:
            raise NotDefined('Comparing unit scalars with differing units'
                             'is not defined')
        return self.value >= other.value

    def __gt__(self, other):
        if self.complex or other.complex:
            raise NotDefined('Comparing complex values is not defined')
        if self.unit != other.unit:
            raise NotDefined('Comparing unit scalars with differing units'
                             'is not defined')
        return self.value > other.value

    def is_scalar(self):
        return self.unit.is_scalar()


@publish_infix(UnitScalar, UnitScalar, 0, '**')
def pow(base, exp):
    if not exp.is_scalar():
        raise NotDefined('The potency is not defined for non scalar exponents')
    if not base.is_scalar() and exp.complex:
        raise NotDefined('Power is not defined for complex exponents '
                         'and basis with units')
    return UnitScalar(mpmath.power(base.value, exp.value),
                      unit_exp(base.unit, exp.value))


@publish_function_named('sqrt', UnitScalar)
def us_sqrt(unit_scalar):
    '''Calculates the square root of a unit scalar'''
    return UnitScalar(mpmath.sqrt(unit_scalar.value),
                      unit_sqrt(unit_scalar.unit))


@publish_function_named('abs', UnitScalar)
def us_abs(unit_scalar):
    '''Calculates the absolute value of a unit scalar'''
    return UnitScalar(abs(unit_scalar.value), unit_scalar.unit)


@publish_function_named('sgnm', UnitScalar)
def us_sgnm(unit_scalar):
    '''Calculates the signum of a unit scalar'''
    return UnitScalar(1, Unit()) if unit_scalar.value > 0\
        else UnitScalar(-1, Unit())
    raise NotDefined('The signum of zero is not defined')
