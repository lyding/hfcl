'''
Every SI-Unit can be described as a vector in the
7 dimensional si-unit vector space, where the base vectors
are the time in seconds, the length in meter, the mass in gramms,,
the electrical current in ampere, the temperature in kelvin,
the amount (stoffmenge) in mol and the intensity of light in candela
Here the units is extended by an exp base vector which represents
exponents to the base of ten like Kilo, Mega, etc...
So the unit class represents an unit as a vector in a 8th
dimensional vectorspace.
'''
from hfcl.core.base_object import Numerical
from hfcl.core.exceptions import hfclException


class MissmatchingUnitsException(hfclException):
    '''
    Raised when two variables have missmatching units
    '''
    def __init__(self, unit1, unit2, *args):
        self.message = f'Missmatching units {unit1} and {unit2}'
        super().__init__(self.message, *args)


# Representation in SI units tends to become long and cumbersome
# (and whats the point of doing physics if you cant name a unit
# after yourself) so there are lots of defacto standart units
# like newton. The extended_base_units is a list of units which
# are used to simplify the pure si unit representation
# Here the order of the extended base units represents the priority
EXTENDED_BASE_UNITS = []


class Unit(Numerical):
    '''Represent a unit as an eighth dimensional vector'''
    def __init__(self, exp=0, time=0, length=0, mass=0, current=0,
                 temperature=0, amount=0, intensity=0,
                 extended_base_units=None, ebuable=False):
        self.repr = (exp, time, length, mass, current,
                     temperature, amount, intensity)
        if extended_base_units is None:
            # Store the extended base units which are used to simplify
            # the si representation
            extended_base_units = EXTENDED_BASE_UNITS
        self.extended_base_units = extended_base_units

        # The extended_repr is the unit expressed in extended base
        # units.
        self.extended_repr = [0 for i in extended_base_units]
        self.ebuable = ebuable
        if self.ebuable:
            self.extended_base_units.append(self)

    def simplify_inplace(self):
        '''
        Uses the extended_base_units list to represent the unit
        using extended base units
        '''
        self.extended_repr = [0 for i in range(len(self.extended_base_units))]
        for i in range(len(self.extended_base_units)):
            extended_base_unit = self.extended_base_units[i]
            quantity = self.__contains(extended_base_unit)
            self.repr = tuple(map(lambda x, y: x - y, self.repr,
                              exp(extended_base_unit, quantity).repr))
            self.extended_repr[i] = quantity

    def expand_inplace(self):
        '''
        Expands the extended_repr to a pure si representation,
        this is the inverse operation of simplify_inplace
        '''
        for i in range(len(self.extended_base_units)):
            extended_base_unit = self.extended_base_units[i]
            quantity = self.extended_repr[i]
            self.repr = tuple(map(lambda x, y: x + y, self.repr,
                              exp(extended_base_unit, quantity).repr))
            self.extended_repr[i] = 0

    def set_extended_base_units(self, extended_base_units, simplify=True):
        '''
        Sets new extended base units:
        :param extended_base_units: The new list of extended base units
        :param simplify=True: Simplifies the unit if set
        '''
        self.expand_inplace()
        self.extended_base_units = extended_base_units
        if simplify:
            self.simplify_inplace()

    def is_scalar(self):
        return self.repr == (0, 0, 0, 0, 0, 0, 0, 0)

    def __str__(self):
        return str(self.repr) + '|' + str(self.extended_repr)

    def __contains(self, _unit):
        '''
        Returns how often self contains _unit
        '''
        quantities = []
        for i in range(len(_unit.repr)):
            if _unit.repr[i] != 0:
                quantities.append(self.repr[i] // _unit.repr[i])
        # If there are negative quantities that means
        # self hase a base unit in the nominator where _unit
        # hase the base unit in the denominator self does not
        # contain _unit
        if list(filter(lambda x: x < 0, quantities)):
            return 0
        # Some base units may be represented more often but the
        # smallest quantity describes how often self contains _unit
        return min(quantities)

    def __eq__(self, other):
        return self.repr == other.repr

    def __add__(unit1, unit2):
        unit1.expand_inplace()
        unit2.expand_inplace()
        if unit1.repr == unit2.repr:
            return unit_from_unit(unit1)
        raise MissmatchingUnitsException(unit1, unit2)

    def __sub__(unit1, unit2):
        unit1.expand_inplace()
        unit2.expand_inplace()
        if unit1.repr == unit2.repr:
            return unit_from_unit(unit1)
        raise MissmatchingUnitsException(unit1, unit2)

    def __mul__(unit1, unit2):
        _repr = []
        unit1.expand_inplace()
        unit2.expand_inplace()
        for i in range(0, 8):
            _repr.append(unit1.repr[i] + unit2.repr[i])
        return unit_from_repr(_repr, join_extended_base_units(
            unit1.extended_base_units, unit2.extended_base_units))

    def __truediv__(unit1, unit2):
        _repr = []
        unit1.expand_inplace()
        unit2.expand_inplace()
        for i in range(0, 8):
            _repr.append(unit1.repr[i] - unit2.repr[i])
        return unit_from_repr(_repr, join_extended_base_units(
            unit1.extended_base_units, unit2.extended_base_units))


# The following functions could be class methodes
# but somehow I did not liked them to be class methodes
def unit_from_unit(unit):
    return Unit(*unit.repr, unit.extended_base_units)


def unit_from_repr(_repr, extended_base_units=EXTENDED_BASE_UNITS):
    return Unit(*_repr, extended_base_units)


def mul(unit1, unit2):
    _repr = []
    unit1.expand_inplace()
    unit2.expand_inplace()
    for i in range(0, 8):
        _repr.append(unit1.repr[i] + unit2.repr[i])
    return unit_from_repr(_repr, join_extended_base_units(
        unit1.extended_base_units, unit2.extended_base_units))


def div(unit1, unit2):
    _repr = []
    unit1.expand_inplace()
    unit2.expand_inplace()
    for i in range(0, 8):
        _repr.append(unit1.repr[i] - unit2.repr[i])
    return unit_from_repr(_repr, join_extended_base_units(
        unit1.extended_base_units, unit2.extended_base_units))


def exp(unit1, exponent):
    # Explicitly convert the exponent to float
    # as mpc is an overkill for exponents
    # Use the real value of the exponent as complex exponents
    # are not defined for units, but this is handeld in the
    # unit scalar module. Here we can assume that the exponent is
    # purely real
    exponent = float(exponent.real)
    _repr = tuple(map(lambda x: x * exponent, unit1.repr))
    return unit_from_repr(_repr, unit1.extended_base_units)


def sqrt(unit1):
    return exp(unit1, 0.5)


def add(unit1, unit2):
    unit1.expand_inplace()
    unit2.expand_inplace()
    if unit1.repr == unit2.repr:
        return unit_from_unit(unit1)
    raise MissmatchingUnitsException(unit1, unit2)


def sub(unit1, unit2):
    unit1.expand_inplace()
    unit2.expand_inplace()
    if unit1.repr == unit2.repr:
        return unit_from_unit(unit1)
    raise MissmatchingUnitsException(unit1, unit2)


def set_default_extended_base_units(extended_base_units):
    for extended_base_unit in extended_base_units:
        if extended_base_unit.extended_base_units:
            raise Exception('Extended base units mus not have'
                            'extended base units on their own!')
    global EXTENDED_BASE_UNITS
    EXTENDED_BASE_UNITS = extended_base_units


def join_extended_base_units(ebus1, ebus2):
    '''
    Join two extended base unit lists together,
    so that the resulting extended base unit list contains
    all extended base units of ebus1 and ebus2
    '''
    # Copy the ebus1 list
    result = list(ebus1)
    for base_unit in ebus2:
        if base_unit in result:
            continue
        result.append(base_unit)
    return result
