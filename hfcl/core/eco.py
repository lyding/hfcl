'''
This module contains mostly helper functions for load hfcl module
some sugar around evaluating a line and so on
'''
import sys
import pkgutil
import importlib
import hfcl.core.lang as lang
import hfcl.core.config as config
from hfcl.core.exceptions import hfclException


# Add the plugin directory to path so the plugins can be imported using
# the import statement.
sys.path.insert(len(sys.path), str(config.PLUGIN_FILE_SUBDIRECTORY))


def error(string):
    print('Error:', string, file=sys.stderr)


def evaluate_hfcl_line(line, verbose=True):
    line = line.strip()
    try:
        transformed_line = lang.PREPROCESSING_PARSER.parse(line)
        if verbose:
            print(lang.EVALUATING_PARSER.parse(transformed_line))
        else:
            lang.EVALUATING_PARSER.parse(transformed_line)
    except hfclException as exception:
        error(exception)
    except ZeroDivisionError:
        error('Division by zero')


def execute_hfcl_module(path, base=config.CONFIG_FILE_SUBDIRECTORY):
    '''
    Execute a hfcl module line by line
    '''
    p = (base / path).with_suffix(config.HFCL_SUFFIX)
    with open(p, 'r') as fd:
        lines = fd.readlines()
        for line in lines:
            evaluate_hfcl_line(line, False)
    print(f'Succesfully loaded: {path}')


def import_hfcl_plugin(plugin_name):
    '''
    Import a python module from basepath
    As hfcl plugins are only python modules,
    this is how plugins are imported
    '''
    # The plugins does not have to be imported, it is enough to execute
    # them. All functions and operators are accessible through the
    # function_pool module
    __import__(plugin_name)
    import_submodules(plugin_name)


def import_submodules(package, recursive=True):
    """ Import all submodules of a module, recursively, including subpackages

    :param package: package (name or actual module)
    :type package: str | module
    :rtype: dict[str, types.ModuleType]
    """
    if isinstance(package, str):
        package = importlib.import_module(package)
    results = {}
    for loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        results[full_name] = importlib.import_module(full_name)
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results
