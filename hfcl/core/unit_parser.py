'''
This module is about parsing units,
this is done with an own grammar and unit evaluator
'''

from lark import Lark
from lark import Transformer
from hfcl.core.unit import (
    Unit,
    exp as unit_exp,
    mul as unit_mul)
from hfcl.core.exceptions import hfclException


__grammar = '''
    ?start: (unit_part)+ -> unit
    ?unit_part: (LETTER | "[" CNAME "]") [exponent] -> unit_part
    ?exponent: "^" SIGNED_NUMBER -> exponent
    %import common.CNAME
    %import common.LETTER
    %import common.WS_INLINE
    %import common.SIGNED_NUMBER
    %ignore WS_INLINE
    '''


class UnitNotDefined(hfclException):
    def __init__(self, unit_name, *args):
        self.message = f'The unit {unit_name} is not defined '\
                       f'use show_units() to see a list of defined units'
        super().__init__(self.message, *args)


class UnitEvaluator(Transformer):
    def __init__(self):
        self.unit_map = {
            'k': Unit(1),
            's': Unit(0, 1),
            'm': Unit(0, 0, 1),
            'g': Unit(0, 0, 0, 1),
            'A': Unit(0, 0, 0, 0, 1),
            'K': Unit(0, 0, 0, 0, 0, 1),
            'mol': Unit(0, 0, 0, 0, 0, 0, 1),
            'Cd': Unit(0, 0, 0, 0, 0, 0, 0, 1),
            'N': Unit(1, -2, 1, 1, 0, 0, 0, 0, [], True)
            }

    def unit(self, args):
        result = Unit()
        for arg in args:
            result = unit_mul(result, arg)
        return result

    def unit_part(self, args):
        exp = 1
        if len(args) > 1:
            # Here the exponent is already converted into float
            exp = args[1]
        try:
            return unit_exp(self.unit_map[str(args[0])], exp)
        except KeyError:
            raise UnitNotDefined(str(args[0]))
        return None

    def exponent(self, args):
        # Allow float, cuz why not?
        return float(args[0])


UNIT_EVALUATOR = UnitEvaluator()
UNIT_PARSER = Lark(__grammar, parser='lalr', transformer=UNIT_EVALUATOR)
