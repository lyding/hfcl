'''
This module contains the repl and is something like the main.py of hfcl
'''
import sys
import hfcl.core.config as config
import hfcl.core.eco as eco
# Load all libraries from the basic subdirectory as this is the
# standard library
from hfcl.basic import *


def main(args):
    # Ignore the cwd path
    config_files = args[1:]
    for config_file in config_files:
        eco.execute_hfcl_module(config_file)
    prompt = config.PROMPT
    while True:
        cmd = input(prompt)
        eco.evaluate_hfcl_line(cmd)


if __name__ == '__main__':
    main(sys.argv)
