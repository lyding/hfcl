'''
This module will become the base object of all datatypes
available in hfcl.
'''
from abc import ABC, abstractmethod
from hfcl.core.function_pool import publish_infix


class BaseObject(ABC):
    pass


class String(BaseObject, str):
    '''
    This baseclass is needed to make str be part of the
    hfcl type system
    '''
    pass


class Numerical(BaseObject):
    '''
    Every class which implements the four basic operations
    should inherit from Numerical
    '''
    @abstractmethod
    def __add__(self, other):
        pass

    @abstractmethod
    def __sub__(self, other):
        pass

    @abstractmethod
    def __mul__(self, other):
        pass

    @abstractmethod
    def __truediv__(self, other):
        pass


class Comparable(BaseObject):
    '''
    Every class which implements the five basic comparison operations
    should inherit from Comparable
    '''
    @abstractmethod
    def __lt__(self, other):
        pass

    @abstractmethod
    def __le__(self, other):
        pass

    @abstractmethod
    def __eq__(self, other):
        pass

    @abstractmethod
    def __ge__(self, other):
        pass

    @abstractmethod
    def __gt__(self, other):
        pass


@publish_infix(Numerical, Numerical, 2, '+')
def add(num1, num2):
    return num1 + num2


@publish_infix(Numerical, Numerical, 2, '-')
def sub(num1, num2):
    return num1 - num2


@publish_infix(Numerical, Numerical, 1, '*')
def mul(num1, num2):
    return num1 * num2


@publish_infix(Numerical, Numerical, 1, '/')
def div(num1, num2):
    return num1 / num2


# Publish standard comparison functions
publish_infix(Comparable, Comparable, 4, '<=')(
    lambda self, other: self <= other)
publish_infix(Comparable, Comparable, 4, '==')(
    lambda self, other: self == other)
publish_infix(Comparable, Comparable, 4, '>=')(
    lambda self, other: self >= other)
publish_infix(Comparable, Comparable, 4, '>')(
    lambda self, other: self > other)
publish_infix(Comparable, Comparable, 4, '<')(
    lambda self, other: self < other)
