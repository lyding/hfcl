'''
This module contains the grammar, the parser and the tree transformer
for the new yet unamed input language.
As the hfcl should be only a calculator for physics the defined
language is nowhere near turing completeness
'''

from lark import Lark, Transformer
from hfcl.basic.unit_scalar import UnitScalar
from hfcl.core.base_object import String
from hfcl.core.unit import Unit
from hfcl.core.unit_parser import UNIT_PARSER
from hfcl.core.exceptions import hfclException
import hfcl.core.function_pool as fp


grammar = '''
    ?start: expr -> evaluate_start
        | CNAME "=" expr -> evaluate_assign
        | "const" CNAME "=" expr -> evaluate_const_assign
    ?expr: expr OPERATOR factor -> evaluate_infix_operation_call
        | factor
    ?factor: NUMBER [unit]-> evaluate_number
        | NUMBER "i" -> evaluate_complex_number
        | "(" expr ")" -> evaluate_parenthesed_expr
        | "(" expr ")" "i" -> evaluate_parenthesed_expr_with_complex_suffix
        | ESCAPED_STRING -> evaluate_escaped_string
        | function
        | variable
    ?function: CNAME parameter_list [unit] -> evaluate_function
        | CNAME parameter_list "i" -> evaluate_function_with_complex_suffix
    ?variable: CNAME -> evaluate_variable
    ?unit: (base_unit)+ -> evaluate_unit
    ?base_unit: LETTER [exponent]  ->evaluate_simple_base_unit
    | "[" CNAME "]" [exponent] -> evaluate_base_unit
    ?exponent: "^" NUMBER -> evaluate_exponent
    parameter_list: "(" [expr] ")" -> evaluate_parameter_list
    OPERATOR: (/[^a-zA-Z0-9()"\\s^]/)+ | " " CNAME " "
    NUMBER: SIGNED_NUMBER | SIGNED_FLOAT
    %import common.CNAME
    %import common.LETTER
    %import common.SIGNED_FLOAT
    %import common.SIGNED_NUMBER
    %import common.ESCAPED_STRING
    %import common.WS_INLINE
    %ignore WS_INLINE
    '''


class VariableNotDefined(hfclException):
    '''
    Is raised when a variable is not defined
    '''
    def __init__(self, name, *args):
        self.message = f'The variable {name} is not defined'
        super().__init__(self.message, *args)


class VariableAlreadyDefined(hfclException):
    '''
    Its raised when a variable is redefined as constant
    '''
    def __init__(self, name, *args):
        self.message = f'The variable {name} is already defined'
        super().__init__(self.message, *args)


class VariableDefinedAsConstant(hfclException):
    '''
    Its raised when a constant variable is reassigned'
    '''
    def __init__(self, name, *args):
        self.message = f'The variable {name} is defined as constant'
        super().__init__(self.message, *args)


class EvaluatePrecedenceFree(Transformer):
    def __init__(self):
        # Stores variables as tuples of the form (value, constant?)
        self.variables = {}

    def evaluate_start(self, args):
        return args[0]

    def evaluate_assign(self, args):
        if args[0] in self.variables and self.variables[args[0]][1]:
            raise VariableDefinedAsConstant(args[0])
        self.variables[str(args[0])] = (args[1], False)
        return args[1]

    def evaluate_const_assign(self, args):
        if args[0] in self.variables and not self.variables[args[0]][1]:
            raise VariableAlreadyDefined(args[0])
        if args[0] in self.variables and self.variables[args[0]][1]:
            raise VariableDefinedAsConstant(args[0])
        self.variables[str(args[0])] = (args[1], True)
        return args[1]

    def evaluate_infix_operation_call(self, args):
        return fp.call_infix_operation(args[1].strip(), args[0], args[2])

    def evaluate_parenthesed_expr(self, args):
        return args[0]

    def evaluate_parenthesed_expr_with_complex_suffix(self, args):
        return args[0]

    def evaluate_number(self, args):
        value = float(args[0])
        unit = Unit()
        if len(args) > 1:
            unit = UNIT_PARSER.parse(args[1])
        return UnitScalar(value, unit)

    def evaluate_complex_number(self, args):
        value = float(args[0]) * complex(0, 1)
        return UnitScalar(value)

    def evaluate_function(self, args):
        params = args[1]
        return fp.call_function(args[0], params)

    def evaluate_function_with_complex_suffix(self, args):
        params = args[1]
        return fp.call_function(args[0], params)

    def evaluate_parameter_list(self, args):
        '''
        Convert the parameters into a list of paramters,
        even if no or just a single parameter is given.
        Converting the parameters in to a list makes the
        call function methode of the function pool way easier
        '''
        if len(args) == 0:
            return []
        parameter = args[0]
        if type(parameter) is list:
            return sum(args, [])
        return [parameter]

    def evaluate_variable(self, args):
        try:
            return self.variables[str(args[0])][0]
        except KeyError:
            raise VariableNotDefined(str(args[0]))

    def evaluate_exponent(self, args):
        return args[0]

    def evaluate_simple_base_unit(self, args):
        if len(args) > 1:
            return args[0] + '^' + args[1]
        return args[0]

    def evaluate_base_unit(self, args):
        if len(args) > 1:
            return '[' + args[0] + ']' + '^' + args[1]
        return '[' + args[0] + ']'

    def evaluate_unit(self, args):
        result = ''
        for arg in args:
            result += arg
        return result

    def evaluate_escaped_string(self, args):
        # Strip the surrounding '"' signs
        return String(args[0][1:-1])


class ParenthesisTransformer(Transformer):
    def evaluate_start(self, args):
        return '((((' + args[0] + '))))'

    def evaluate_assign(self, args):
        return args[0] + '=((((' + args[1] + '))))'

    def evaluate_const_assign(self, args):
        return 'const ' + args[0] + '=((((' + args[1] + '))))'

    def evaluate_number(self, args):
        '''
        Just returns the representing string
        '''
        if len(args) > 1:
            return args[0] + args[1]
        return args[0]

    def evaluate_complex_number(self, args):
        '''
        Just returns the representing string
        '''
        # Add the 'i' manually again as it is an unnamed terminal
        # within the grammar and therefor is swallowed by the parser
        return args[0] + 'i'

    def evaluate_function(self, args):
        return args[0] + args[1]

    def evaluate_function_with_complex_suffix(self, args):
        return args[0] + args[1] + '*1i'

    def evaluate_parameter_list(self, args):
        if len(args) > 0:
            result = '(((((' + args[0] + ')))))'
        else:
            result = '()'
        return result

    def evaluate_variable(self, args):
        return str(args[0])

    def evaluate_infix_operation_call(self, args):
        '''
        Return the operators name pre- and postpended with the
        right amount of parenthesis according to the operators
        precedence
        '''
        # Here the biggest restriction on infix operations kick in
        # infix operations can be overloaded, but every implementation
        # has to have the same operator precedence.
        # This might change in the future
        # But for now it the first implementation is used to get
        # the operators precedence
        p = list(fp.INFIX_OPERATIONS[args[1].strip()].items())[0][1][1]
        return args[0] + ')' * p + args[1] + '(' * p + args[2]

    def evaluate_parenthesed_expr(self, args):
        '''
        Return the operators name pre- and postpended with the
        right amount of parenthesis according to the operators
        precedence
        '''
        return '((((' + args[0] + '))))'

    def evaluate_parenthesed_expr_with_complex_suffix(self, args):
        '''
        Return the operators name pre- and postpended with the
        right amount of parenthesis according to the operators
        precedence
        '''
        return '((((' + args[0] + '))))' + "*1i"

    def evaluate_exponent(self, args):
        return args[0]

    def evaluate_simple_base_unit(self, args):
        if len(args) > 1:
            return args[0] + '^' + args[1]
        return args[0]

    def evaluate_base_unit(self, args):
        if len(args) > 1:
            return '[' + args[0] + ']' + '^' + args[1]
        return '[' + args[0] + ']'

    def evaluate_unit(self, args):
        result = ''
        for arg in args:
            result += arg
        return result

    def evaluate_escaped_string(self, args):
        return args[0]


PRECEDENCE_FREE_TRANSFORMER = EvaluatePrecedenceFree()
EVALUATING_PARSER = Lark(grammar, parser='lalr',
                         transformer=PRECEDENCE_FREE_TRANSFORMER)
PREPROCESSING_PARSER = Lark(grammar, parser='lalr',
                            transformer=ParenthesisTransformer())
