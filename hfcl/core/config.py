'''
This module contains core configurations not many yet
'''
from pathlib import Path
BASE_PATH = Path.home()
CONFIG_FILE_SUBDIRECTORY = BASE_PATH / Path('.config/hfcl/scripts/')
PLUGIN_FILE_SUBDIRECTORY = BASE_PATH / Path('.config/hfcl/plugins/')
HFCL_SUFFIX = '.hfcl'
PROMPT = 'hfcl> '
