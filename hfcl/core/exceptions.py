'''
This module contains the definition of a hfcl exception.
A hfcl exception is an exception which is raised by the core of hfcl
or one of hfcl libraries.
Hfcl exceptions do not break the repl and therefor must not be fatal.
This module also includes common hfclExceptions
'''


class hfclException(Exception):
    '''
    This is the base class, all hfcl eceptions are handeld
    inside the repl by producing an error message for the user
    '''
    pass


class fatalException(Exception):
    '''
    fatalExceptions are never handeld and thereof always break the repl.
    They are raised inside the core of hfcl and should not be raised
    inside on of hfcl libraries (except there is a realy good reason,
    which I cant think about right now)
    '''
    pass


class NotDefined(hfclException):
    '''
    Is raised when something is not defined in a mathematic sense
    Checkout hfcl/basic/unit_scalar if you want to see examples
    '''
    pass
